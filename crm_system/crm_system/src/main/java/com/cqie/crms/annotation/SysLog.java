package com.cqie.crms.annotation;

import java.lang.annotation.*;

/**
 * @Author: alex
 * @Date: 2021/09/03/16:17
 * @Description:
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";
}
