package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: alex
 * @Date: 2021/09/16/23:28
 * @Description:
 */
@Data
@ApiModel("新增角色入参实体")
public class AddRoleReqBo {
    /**
     * ID
     */
    Long id;
    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty(required = true,value = "角色名")
    String name;

    @NotBlank(message = "备注不能为空")
    @ApiModelProperty(required = true,value = "菜单名")
    String remark;

    /**
     * 菜单路由
     */
    Long[] role;

}
