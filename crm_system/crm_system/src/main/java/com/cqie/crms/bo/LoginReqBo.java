package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/06/9:09
 * @Description:
 */
@Data
@ApiModel("登录入参")
public class LoginReqBo {
    @ApiModelProperty(required = true,value = "登陆账号")
    String account;
    @ApiModelProperty(required = true,value = "登陆密码")
    String password;
}
