package com.cqie.crms.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cqie.crms.pojo.SysMenu;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: alex
 * @Date: 2021/09/17/10:40
 * @Description:
 */
@Data
@ApiModel("菜单信息入参")
public class MenuInfoReqBo {
    /**
     * 菜单id
     */
    private Long id;

    @NotBlank(message = "菜单名不能为空")
    @ApiModelProperty(required = true,value = "菜单名")
    private String name;

    @NotBlank(message = "菜单地址不能为空")
    @ApiModelProperty(required = true,value = "菜单地址")
    private String appUrl;

    @NotBlank(message = "菜单图标不能为空")
    @ApiModelProperty(required = true,value = "菜单图标")
    private String imgUrl;

    /**
     * 排序码
     */
    private Integer sortCode;

    /**
     * 上级菜单Id
     */
    private Long parentId;

}
