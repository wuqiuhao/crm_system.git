package com.cqie.crms.bo;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PageReqBo implements Serializable {

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;
    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /** 排序列 */
    private String orderByColumn;

    /** 排序的方向desc或者asc */
    private String isAsc = "asc";

    public String getOrderBy() {
        if (StrUtil.isEmpty(orderByColumn)) {
            return "";
        }
        return StrUtil.toUnderlineCase(orderByColumn) + " " + (StrUtil.isBlank(isAsc) ? "asc" : isAsc);
    }
}
