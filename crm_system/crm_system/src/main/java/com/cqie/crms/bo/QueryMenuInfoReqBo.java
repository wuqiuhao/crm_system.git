package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/15/14:22
 * @Description:
 */
@Data
@ApiModel("查询菜单列表参数入参")
public class QueryMenuInfoReqBo extends PageReqBo{
    /**
     * 菜单名
     */
    String name;
    /**
     * 菜单名
     */
    String parentId;

}
