package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/14/8:37
 * @Description:
 */
@Data
@ApiModel("查询角色列表参数入参")
public class QueryRoleInfoReqBo extends PageReqBo{

    /**
     * 姓名
     */
    String name;

    /**
     * 状态 0：正常 1：禁用
     */
    Integer status;
}
