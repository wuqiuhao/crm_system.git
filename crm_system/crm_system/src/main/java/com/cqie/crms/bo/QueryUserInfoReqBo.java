package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/07/17:34
 * @Description:
 */
@Data
@ApiModel("查询用户列表参数入参")
public class QueryUserInfoReqBo extends PageReqBo{

    /**
     * 姓名
     */
    String name;

    /**
     * 账号
     */
    String account;

    /**
     * 状态 0：正常 1：禁用
     */
    Integer status;

    /**
     * 用户身份 1：管理员 2：普通用户
     */
    Integer roles;

    /**
     * 用户身份 1：管理员 2：普通用户
     */
    Integer sort;

}
