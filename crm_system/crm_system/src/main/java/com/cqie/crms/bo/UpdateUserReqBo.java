package com.cqie.crms.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: alex
 * @Date: 2021/09/12/9:16
 * @Description:
 */
@Data
@ApiModel("新增用户入参")
public class UpdateUserReqBo {

    @NotNull(message = "用户ID不能为空")
    @ApiModelProperty(required = true, value = "用户ID")
    private Long id;

    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(required = true,value = "用户名")
    String name;

    @NotBlank(message = "账号不能为空")
    @ApiModelProperty(required = true,value = "登陆账号")
    String account;

    @NotEmpty(message = "角色不能为空")
    @ApiModelProperty(required = true,value = "角色")
    Long[] rolesId;

    @NotNull(message = "状态不能为空")
    @ApiModelProperty(required = true,value = "状态")
    Integer status;
}
