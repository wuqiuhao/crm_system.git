package com.cqie.crms.config;

import com.cqie.crms.interceptor.TokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: alex
 * @Date: 2021/09/07/17:29
 * @Description:
 */
@Configuration
public class InterceptConfig extends WebMvcConfigurationSupport {

    private TokenInterceptor tokenInterceptor;

    //构造方法
    public InterceptConfig(TokenInterceptor tokenInterceptor) {
        this.tokenInterceptor = tokenInterceptor;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> excludePath = new ArrayList<>();
        excludePath.add("/sysUser/login"); //登录
//        excludePath.add("/logout"); //登出
        excludePath.add("/static/**");  //静态资源
        excludePath.add("/assets/**");  //静态资源
        // swagger过滤
        excludePath.add("/favicon.ico");
        excludePath.add("/actuator/health/**");
        excludePath.add("/v2/api-docs/**");
        excludePath.add("/v2/api-docs-ext/**");
        excludePath.add("/swagger-resources");
        excludePath.add("/error/**");
        excludePath.add("/webjars/**");
        excludePath.add("/swagger-ui.html");
        excludePath.add("/doc.html");

        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(excludePath);
        super.addInterceptors(registry);
    }

}
