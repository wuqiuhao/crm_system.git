package com.cqie.crms.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色菜单权限表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-16
 */
@RestController
@RequestMapping("/roleMenu")
public class RoleMenuController {

}

