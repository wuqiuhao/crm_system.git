package com.cqie.crms.controller;


import com.cqie.crms.pojo.RoleMenuView;
import com.cqie.crms.pojo.SysMenu;
import com.cqie.crms.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * VIEW 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-17
 */
@RestController
@RequestMapping("/roleMenuView")
public class RoleMenuViewController {

}

