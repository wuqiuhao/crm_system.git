package com.cqie.crms.controller;


import com.cqie.crms.bo.MenuInfoReqBo;
import com.cqie.crms.bo.QueryMenuInfoReqBo;
import com.cqie.crms.bo.QueryRoleInfoReqBo;
import com.cqie.crms.dto.UserContext;
import com.cqie.crms.pojo.RoleMenu;
import com.cqie.crms.pojo.RoleMenuView;
import com.cqie.crms.pojo.SysMenu;
import com.cqie.crms.service.IRoleMenuService;
import com.cqie.crms.service.IRoleMenuViewService;
import com.cqie.crms.service.ISysMenuService;
import com.cqie.crms.service.IUserInfoViewService;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.vo.MenuJsonVo;
import com.cqie.crms.vo.MenuVo;
import com.cqie.crms.vo.ResultVo;
import com.cqie.crms.vo.UserVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-14
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/sysMenu")
public class SysMenuController {

    @Autowired
    private ISysMenuService iSysMenuService;

    @Autowired
    private IRoleMenuService iRoleMenuService;

    @Autowired
    private IRoleMenuViewService iRoleMenuViewService;

    @Autowired
    private IUserInfoViewService iUserInfoViewService;

    @ApiOperation("创建树形菜单")
    @GetMapping(value = "/buildMenu")
    public ResultVo<SysMenu> getRoleList(){
        List<SysMenu> menuVos = iSysMenuService.buildMenu();
        return ResultVo.success("查询成功",menuVos);
    }

    @ApiOperation("创建动态路由")
    @GetMapping(value = "/queryRouteByRole")
    public ResultVo<RoleMenuView> queryRouteByRole(){
        //根据Token查询用户ID
        Long userId = UserContext.get().getId();
        //根据ID查询用户角色
        UserVo userVo = iUserInfoViewService.getInfoById(userId);
        System.out.println(userVo.getRolesId());
        List<RoleMenuView> roleMenuViews = iRoleMenuViewService.queryRouteByRole(userVo.getRolesId());
        return ResultVo.success("查询成功",roleMenuViews);
    }

    @ApiOperation("查询菜单列表")
    @GetMapping(value = "/getMenuList")
    public ResultVo<PageInfo<SysMenu>> getMenuList(QueryMenuInfoReqBo bo){
        List<SysMenu> menuVos = iSysMenuService.getMenuList(bo);
        return ResultVo.success("查询成功",MyPageHelper.returnPage(menuVos));
    }

    @ApiOperation("根据角色查询菜单权限")
    @GetMapping(value = "/getMenuListByRole")
    public ResultVo getMenuListByRole(String roleId){
        String arr = iRoleMenuService.getMenuListByRole(roleId);
        return ResultVo.success("查询成功",arr);
    }

    @ApiOperation("新增菜单")
    @PostMapping(value = "/addMenuInfo")
    public ResultVo addMenuInfo(@RequestBody @Valid MenuInfoReqBo bo){
        Boolean bln = iSysMenuService.addMenu(bo);
        AssertUtil.isTrue(bln,"新增菜单失败！");
        return ResultVo.success();
    }

    @ApiOperation("编辑菜单")
    @PostMapping(value = "/updateMenuInfo")
    public ResultVo updateMenuInfo(@RequestBody @Valid MenuInfoReqBo bo){
        Boolean bln = iSysMenuService.updateMenu(bo);
        AssertUtil.isTrue(bln,"编辑菜单失败！");
        return ResultVo.success();
    }

    @ApiOperation("删除菜单")
    @PostMapping(value = "/deleteMenuInfo")
    public ResultVo deleteMenuInfo(@RequestBody String[] menuId){
        Boolean bln = iSysMenuService.deleteMenu(menuId);
        AssertUtil.isTrue(bln,"删除失败！");
        return ResultVo.success();
    }
}

