package com.cqie.crms.controller;


import com.cqie.crms.bo.AddRoleReqBo;
import com.cqie.crms.bo.QueryRoleInfoReqBo;
import com.cqie.crms.service.ISysRoleService;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.vo.ResultVo;
import com.cqie.crms.vo.RoleVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-09
 */
@Api(tags = "角色管理")
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private ISysRoleService iSysRoleService;

    @ApiOperation("查询下拉角色列表")
    @GetMapping(value = "/getRoleList")
    public ResultVo<PageInfo<RoleVo>> getRoleList(QueryRoleInfoReqBo bo){
        List<RoleVo> roleVos = iSysRoleService.getRoleList(bo);
        return ResultVo.success(MyPageHelper.returnPage(roleVos));
    }

    @ApiOperation("新增角色")
    @PostMapping(value = "/addRole")
    public ResultVo addRole(@RequestBody AddRoleReqBo bo){
        Boolean bln = iSysRoleService.addRole(bo);
        AssertUtil.isTrue(bln,"新增角色失败！");
        return ResultVo.success();
    }

    @ApiOperation("编辑角色")
    @PostMapping(value = "/updateRole")
    public ResultVo updateRole(@RequestBody AddRoleReqBo bo){
        System.out.println(bo);
        Boolean bln = iSysRoleService.updateRole(bo);
        AssertUtil.isTrue(bln,"编辑角色失败！");
        return ResultVo.success();
    }

    @ApiOperation("删除角色")
    @PostMapping(value = "/deleteRole")
    public ResultVo deleteRole(@RequestParam("id") String id){
        Boolean bln = iSysRoleService.deleteRole(id);
        AssertUtil.isTrue(bln,"删除角色失败！");
        return ResultVo.success();
    }
}

