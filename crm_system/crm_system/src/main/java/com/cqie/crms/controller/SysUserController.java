package com.cqie.crms.controller;


import com.cqie.crms.bo.AddUserReqBo;
import com.cqie.crms.bo.LoginReqBo;
import com.cqie.crms.bo.UpdateUserReqBo;
import com.cqie.crms.dto.UserContext;
import com.cqie.crms.service.ISysUserService;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.vo.LoginVo;
import com.cqie.crms.vo.ResultVo;
import com.cqie.crms.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-02
 */
@Api(tags = "用户信息管理")
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private ISysUserService userService;

    @ApiOperation("登入")
    @PostMapping(value = "/login")
    public ResultVo<LoginVo> Login(@RequestBody LoginReqBo bo){
        System.out.println(bo.getAccount());
        System.out.println(bo.getPassword());
        LoginVo login = userService.login(bo);
        System.out.println("token="+login);
        return ResultVo.success("Success",login);
    }

    @ApiOperation("登出")
    @PostMapping(value = "/logout")
    public ResultVo logout(){
        return ResultVo.success();
    }

    @ApiOperation("获取登录用户信息")
    @GetMapping(value = "/getInfo")
    public ResultVo<UserVo> getUserInfo() {
        System.out.println(UserContext.get().getId());
        UserVo userInfo = userService.getInfoById(UserContext.get().getId());
        return ResultVo.success(userInfo);
    }

    @ApiOperation("新增用户")
    @PostMapping(value = "/addUserinfo")
    public ResultVo addUserInfo(@RequestBody @Valid AddUserReqBo bo) {
        System.out.println(bo);
        Boolean bln = userService.addUser(bo);
        AssertUtil.isTrue(bln,"新增用户失败！");
        return ResultVo.success();
    }

    @ApiOperation("编辑用户")
    @PostMapping(value = "/updateUserinfo")
    public ResultVo updateUserinfo(@RequestBody @Valid UpdateUserReqBo bo) {
        System.out.println("用户:"+bo.getName());
        System.out.println("角色："+bo.getRolesId());
        Boolean bln = userService.updateUser(bo);
        AssertUtil.isTrue(bln,"编辑用户信息失败！");
        return ResultVo.success();
    }

    @ApiOperation("删除用户（批量）")
    @PostMapping(value = "/deleteUserinfo")
    public ResultVo deleteUserinfo(@RequestBody String[] userId) {
        System.out.println("用户ID:"+userId);
        Boolean bln = userService.deleteUserById(userId);
        AssertUtil.isTrue(bln,"删除用户信息失败！");
        return ResultVo.success();
    }

    @ApiOperation("修改用户状态")
    @PostMapping(value = "/modifyStatus")
    public ResultVo modifyUserStatus( String id,String status) {
        System.out.println("用户ID:"+id);
        System.out.println("用户状态:"+status);
        Boolean bln = userService.modifyStatus(id,status);
        AssertUtil.isTrue(bln,"修改用户状态失败！");
        return ResultVo.success();
    }
//    @ApiOperation("获取用户列表")
//    @GetMapping(value = "/getUserList")
//    public ResultVo<PageInfo<UserVo>> getPageList(QueryUserInfoReqBo bo) {
//        List<UserVo> userVoList = userService.getUserList(bo);
//        return ResultVo.success(MyPageHelper.returnPage(userVoList));
//    }
}

