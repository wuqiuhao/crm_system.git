package com.cqie.crms.controller;


import com.cqie.crms.bo.QueryUserInfoReqBo;
import com.cqie.crms.dto.UserContext;
import com.cqie.crms.service.IUserInfoViewService;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.vo.ResultVo;
import com.cqie.crms.vo.UserVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * VIEW 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-08
 */
@Api(tags = "用户管理视图")
@RestController
@RequestMapping("/userInfoView")
public class UserInfoViewController {
    @Autowired
    private IUserInfoViewService iUserInfoViewService;

    @ApiOperation("获取登录用户信息")
    @GetMapping(value = "/getInfo")
    public ResultVo<UserVo> getUserInfo() {
        System.out.println(UserContext.get().getId());
        UserVo userInfo = iUserInfoViewService.getInfoById(UserContext.get().getId());
        return ResultVo.success(userInfo);
    }

    @ApiOperation("获取用户列表")
    @GetMapping(value = "/getUserList")
    public ResultVo<PageInfo<UserVo>> getPageList(QueryUserInfoReqBo bo) {
        List<UserVo> userVoList = iUserInfoViewService.getUserList(bo);
        return ResultVo.success(MyPageHelper.returnPage(userVoList));
    }
}

