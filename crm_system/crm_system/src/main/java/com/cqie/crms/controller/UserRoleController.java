package com.cqie.crms.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色关系表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2021-09-10
 */
@RestController
@RequestMapping("/userRole")
public class UserRoleController {

}

