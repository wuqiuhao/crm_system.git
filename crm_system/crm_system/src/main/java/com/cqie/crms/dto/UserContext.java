package com.cqie.crms.dto;


import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.vo.UserVo;

public class UserContext {

    private static ThreadLocal<UserVo> userThread = new ThreadLocal<>();

    public static void set(UserVo user) {

        userThread.set(user);
    }

    public static UserVo get() {
        UserVo userVo = userThread.get();
        AssertUtil.isNotNull(userVo, "用户未登录");
        return userVo;
    }

    //防止内存泄漏
    public static void remove() {

        userThread.remove();
    }
}
