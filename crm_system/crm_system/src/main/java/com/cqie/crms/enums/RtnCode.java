package com.cqie.crms.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * @Author: alex
 * @Date: 2021/09/06/9:05
 * @Description:
 */
public enum RtnCode {
    SUCCESS("20000", "成功"),

    ERR_PARAMS("100000", "提交参数错误"),
    //
    ROLE_NAME_EXIST("100001", "角色名已存在!")
    //
    , ROLE_NOTEXIST("100002", "角色不存在!")
    //
    , ERR_USERNAMEORPWD("110001", "用户名或密码错误！")
    //
    , ERR_INVALID_USER("110002", "用户登录信息已过期，请重新登录！")
    //
    , ERR_ADDUSER_ROLE("110003", "没有权限在该角色添加用户！")
    //
    , ERR_USERROLE_EXIST("110004", "用户关联角色已存在，无需重复添加！")
    //
    , ERR_ADDUSER_EXP("110005", "添加用户异常！")
    //
    , ERR_ACCESS("999997", "没有访问系统的权限！"),
    //
    PERMISSION_DENIED("999998", "权限不足"),
    //
    UNKNOWN_EXCEPTION("999999", "未知异常");

    private String desc;
    private String code;

    RtnCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

    public static RtnCode getByCode(String code) {
        Optional<RtnCode> found = Arrays.stream(RtnCode.values()).filter(it -> it.getCode().equals(code)).findFirst();
        return found.isPresent() ? found.get() : UNKNOWN_EXCEPTION;
    }
}

