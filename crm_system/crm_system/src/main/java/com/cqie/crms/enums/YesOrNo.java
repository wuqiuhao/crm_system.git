package com.cqie.crms.enums;

import com.cqie.crms.utils.AssertUtil;
import lombok.Getter;

import java.util.Arrays;

/**
 * @Author: alex
 * @Date: 2021/09/07/19:53
 * @Description: 真假枚举
 */
@Getter
public enum YesOrNo {

    NO("1", "否/停用"), YES("0", "是/启用"),;

    private final String code;
    private final String desc;

    YesOrNo(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static YesOrNo getByCode(String code) {
        AssertUtil.isNotBlank(code, "枚举参数错误");
        YesOrNo[] values = values();
        return Arrays.stream(values).filter(it -> it.code.equals(code)).findAny().orElseGet(null);
    }

}
