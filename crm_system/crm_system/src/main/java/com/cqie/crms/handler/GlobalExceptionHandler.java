package com.cqie.crms.handler;

import cn.hutool.core.lang.Validator;
import cn.hutool.http.HttpStatus;

import com.cqie.crms.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.security.auth.login.AccountExpiredException;
import javax.validation.ConstraintViolationException;
import java.nio.file.AccessDeniedException;

/**
 * 全局异常处理器
 *
 * @author plastic
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 业务异常
     */
    @ExceptionHandler(CustomException.class)
    public ResultVo businessException(CustomException e) {
        log.warn("CustomException error:", e);
        if (Validator.isNull(e.getCode())) {
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.error(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResultVo handlerNoFoundException(Exception e) {
        log.error(e.getMessage(), e);
        return ResultVo.error(String.valueOf(HttpStatus.HTTP_NOT_FOUND), "路径不存在，请检查路径是否正确");
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResultVo handleAuthorizationException(AccessDeniedException e) {
        log.error(e.getMessage());
        return ResultVo.error(String.valueOf(HttpStatus.HTTP_FORBIDDEN), "没有权限，请联系管理员授权");
    }

    @ExceptionHandler(AccountExpiredException.class)
    public ResultVo handleAccountExpiredException(AccountExpiredException e) {
        log.error(e.getMessage(), e);
        return ResultVo.error(e.getMessage());
    }


    @ExceptionHandler(Exception.class)
    public ResultVo handleException(Exception e) {
        log.error(e.getMessage(), e);
        return ResultVo.error(e.getMessage());
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(BindException.class)
    public ResultVo validatedBindException(BindException e) {
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return ResultVo.error(message);
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResultVo constraintViolationException(ConstraintViolationException e) {
        log.error(e.getMessage(), e);
        String message = e.getConstraintViolations().iterator().next().getMessage();
        return ResultVo.error(message);
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object validExceptionHandler(MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        String message = e.getBindingResult().getFieldError().getDefaultMessage();
        return ResultVo.error(message);
    }

}
