package com.cqie.crms.mapper;

import com.cqie.crms.pojo.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 角色菜单权限表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-16
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    @Select("select GROUP_CONCAT(menuId) as menuId FROM role_menu WHERE roleId in (${roleId})")
    String getMenuListByRole(@Param("roleId") String roleId);

}
