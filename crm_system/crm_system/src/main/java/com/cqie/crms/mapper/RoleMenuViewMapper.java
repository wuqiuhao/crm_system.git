package com.cqie.crms.mapper;

import com.cqie.crms.pojo.RoleMenuView;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqie.crms.vo.RouteVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-17
 */
public interface RoleMenuViewMapper extends BaseMapper<RoleMenuView> {

    @Select({"<script>" +
            "SELECT DISTINCT\n" +
            "\trole_menu_view.menu_id,\n" +
            "\trole_menu_view.menu_name,\n" +
            "\trole_menu_view.appUrl,\n" +
            "\trole_menu_view.imgUrl,\n" +
            "\trole_menu_view.sortCode,\n" +
            "\trole_menu_view.parentId \n" +
            "FROM\n" +
            "\trole_menu_view \n" +
            "WHERE\n" +
            "\trole_id in (${rolesId})\n" +
            "</script>"})
    List<RouteVo> getMenuByRole(@Param("rolesId") String rolesId);

}
