package com.cqie.crms.mapper;

import com.cqie.crms.pojo.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-14
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
