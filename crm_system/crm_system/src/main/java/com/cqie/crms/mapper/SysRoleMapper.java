package com.cqie.crms.mapper;

import com.cqie.crms.pojo.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-09
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
