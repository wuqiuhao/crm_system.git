package com.cqie.crms.mapper;

import com.cqie.crms.pojo.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-02
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
