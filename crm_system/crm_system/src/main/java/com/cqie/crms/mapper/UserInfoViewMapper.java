package com.cqie.crms.mapper;

import com.cqie.crms.pojo.UserInfoView;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-08
 */
public interface UserInfoViewMapper extends BaseMapper<UserInfoView> {

}
