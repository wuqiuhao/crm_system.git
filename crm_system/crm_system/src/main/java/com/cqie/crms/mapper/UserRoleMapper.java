package com.cqie.crms.mapper;

import com.cqie.crms.pojo.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2021-09-10
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
