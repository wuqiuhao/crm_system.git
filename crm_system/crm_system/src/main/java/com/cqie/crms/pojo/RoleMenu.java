package com.cqie.crms.pojo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色菜单权限表
 * </p>
 *
 * @author alex
 * @since 2021-09-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RoleMenu extends Model {

    private static final long serialVersionUID=1L;

    /**
     * 角色id
     */
    @TableField("menuId")
    private Long menuId;

    /**
     * 菜单id
     */
    @TableField("roleId")
    private Long roleId;


}
