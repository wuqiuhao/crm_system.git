package com.cqie.crms.pojo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * VIEW
 * </p>
 *
 * @author alex
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RoleMenuView extends Model {

    private static final long serialVersionUID=1L;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 菜单id
     */
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 程序路径
     */
    @TableField("appUrl")
    private String appUrl;

    /**
     * 图标路径
     */
    @TableField("imgUrl")
    private String imgUrl;

    /**
     * 排序码
     */
    @TableField("sortCode")
    private Integer sortCode;

    /**
     * 上级菜单Id
     */
    @TableField("parentId")
    private Long parentId;

    /**
     * 子菜单
     */
    @TableField(exist=false)
    private List<RoleMenuView> children;

}
