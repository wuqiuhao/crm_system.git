package com.cqie.crms.pojo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户角色关系表
 * </p>
 *
 * @author alex
 * @since 2021-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserRole extends Model {

    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    @TableField("userId")
    private Long userId;

    /**
     * 角色id
     */
    @TableField("roleId")
    private Long roleId;


}
