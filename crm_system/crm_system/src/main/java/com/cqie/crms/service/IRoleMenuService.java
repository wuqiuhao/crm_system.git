package com.cqie.crms.service;

import com.cqie.crms.pojo.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.RoleMenuVo;
import org.apache.el.parser.BooleanNode;

import java.util.List;

/**
 * <p>
 * 角色菜单权限表 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-16
 */
public interface IRoleMenuService extends IService<RoleMenu> {

    //根据用户角色查询菜单
    String getMenuListByRole(String roleId);

    //新增角色赋予权限
    Boolean addRoleMenu(RoleMenuVo vo);

    //编辑权限修改权限
    Boolean updateRoleMenu(RoleMenuVo vo);
}
