package com.cqie.crms.service;

import com.cqie.crms.pojo.RoleMenuView;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.RoleMenuVo;
import com.cqie.crms.vo.RouteVo;

import java.util.List;

/**
 * <p>
 * VIEW 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-17
 */
public interface IRoleMenuViewService extends IService<RoleMenuView> {

    //根据角色生成动态路由
    List<RoleMenuView> queryRouteByRole(String rolesId);

    void queryChildren(RoleMenuView roleMenuView);

}
