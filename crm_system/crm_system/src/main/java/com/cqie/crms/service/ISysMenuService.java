package com.cqie.crms.service;

import com.cqie.crms.bo.MenuInfoReqBo;
import com.cqie.crms.bo.QueryMenuInfoReqBo;
import com.cqie.crms.pojo.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-14
 */
public interface ISysMenuService extends IService<SysMenu> {

    //创建权限菜单
    List<SysMenu> buildMenu();

    //查找子菜单
    void findAllChild(SysMenu sysMenu);

    //查询菜单列表
    List<SysMenu> getMenuList(QueryMenuInfoReqBo bo);

    //新增菜单
    Boolean addMenu(MenuInfoReqBo bo);

    //编辑菜单
    Boolean updateMenu(MenuInfoReqBo bo);

    //删除菜单
    Boolean deleteMenu(String[] menuId);
}
