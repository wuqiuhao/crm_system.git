package com.cqie.crms.service;

import com.cqie.crms.bo.AddRoleReqBo;
import com.cqie.crms.bo.QueryRoleInfoReqBo;
import com.cqie.crms.pojo.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.RoleVo;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-09
 */
public interface ISysRoleService extends IService<SysRole> {

    //查询角色下拉框列表
    List<RoleVo> getRoleList(QueryRoleInfoReqBo bo);

    //新增角色
    Boolean addRole(AddRoleReqBo bo);

    //编辑角色
    Boolean updateRole(AddRoleReqBo bo);

    //删除角色
    Boolean deleteRole(String id);
}
