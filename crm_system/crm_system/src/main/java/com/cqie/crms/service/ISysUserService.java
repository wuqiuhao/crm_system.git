package com.cqie.crms.service;

import com.cqie.crms.bo.AddUserReqBo;
import com.cqie.crms.bo.LoginReqBo;
import com.cqie.crms.bo.QueryUserInfoReqBo;
import com.cqie.crms.bo.UpdateUserReqBo;
import com.cqie.crms.pojo.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.LoginVo;
import com.cqie.crms.vo.UserVo;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-02
 */
public interface ISysUserService extends IService<SysUser> {

    //登录
    LoginVo login(LoginReqBo bo);
    //登出
    void logOut();
    //获取用户信息
    UserVo getInfoById(Long userId);
    //获取用户列表
    List<UserVo> getUserList(QueryUserInfoReqBo bo);
    //新增用户到sysUser表
    Boolean addUser(AddUserReqBo bo);
    //编辑用户信息
    Boolean updateUser(UpdateUserReqBo bo);
    //删除用户
    Boolean deleteUserById(String[] userId);
    //修改用户状态
    Boolean modifyStatus(String id,String status);
}
