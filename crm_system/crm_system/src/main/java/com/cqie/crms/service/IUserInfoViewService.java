package com.cqie.crms.service;

import com.cqie.crms.bo.QueryUserInfoReqBo;
import com.cqie.crms.pojo.UserInfoView;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.UserVo;

import java.util.List;

/**
 * <p>
 * VIEW 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-08
 */
public interface IUserInfoViewService extends IService<UserInfoView> {
    //获取用户信息
    UserVo getInfoById(Long userId);
    //获取用户列表
    List<UserVo> getUserList(QueryUserInfoReqBo bo);
}
