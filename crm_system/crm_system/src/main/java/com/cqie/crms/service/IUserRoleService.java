package com.cqie.crms.service;

import com.cqie.crms.pojo.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cqie.crms.vo.UserRoleVo;
import com.cqie.crms.vo.UserVo;
import org.apache.catalina.User;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author alex
 * @since 2021-09-10
 */
public interface IUserRoleService extends IService<UserRole> {

    //新增用户关联角色
    Boolean insertUserRole(UserRoleVo vo);
    //编辑用户关联角色
    Boolean updateUserRole(UserRoleVo vo);
    //删除角色将用户角色设置为游客身份
    Boolean deleteUserRole(Long roleId);
}
