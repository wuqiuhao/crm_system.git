package com.cqie.crms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cqie.crms.pojo.RoleMenu;
import com.cqie.crms.mapper.RoleMenuMapper;
import com.cqie.crms.pojo.UserRole;
import com.cqie.crms.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.vo.RoleMenuVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 角色菜单权限表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-16
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Override
    public String getMenuListByRole(String roleId) {
       String arr = roleMenuMapper.getMenuListByRole(roleId);
        return arr;
    }

    @Override
    public Boolean addRoleMenu(RoleMenuVo vo) {
        Long roleId = vo.getRoleId();
        Long[] menus = vo.getMenus();
        RoleMenu roleMenu = new RoleMenu();
        roleMenu.setRoleId(roleId);
        int i;
        for (i = 0; i < menus.length;i++){
            Long menu = menus[i];
            roleMenu.setMenuId(menu);
            baseMapper.insert(roleMenu);
        }
        if (i == menus.length){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public Boolean updateRoleMenu(RoleMenuVo vo) {
        //获取id
        Long roleId = vo.getRoleId();
        Long[] menus = vo.getMenus();
        //先将该用户原有的角色菜单删除
        baseMapper.delete(new LambdaUpdateWrapper<RoleMenu>().eq(RoleMenu::getRoleId,roleId));
        //添加新的菜单
        RoleMenu roleMenu = new RoleMenu();
        roleMenu.setRoleId(roleId);
        int i;
        for (i = 0; i < menus.length;i++){
            Long menu = menus[i];
            roleMenu.setMenuId(menu);
            baseMapper.insert(roleMenu);
        }
        if (i == menus.length){
            return true;
        }else {
            return false;
        }
    }
}
