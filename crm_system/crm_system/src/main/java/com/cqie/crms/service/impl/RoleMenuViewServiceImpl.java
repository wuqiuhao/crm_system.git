package com.cqie.crms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cqie.crms.pojo.RoleMenuView;
import com.cqie.crms.mapper.RoleMenuViewMapper;
import com.cqie.crms.service.IRoleMenuViewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.vo.RouteVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * VIEW 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-17
 */
@Service
public class RoleMenuViewServiceImpl extends ServiceImpl<RoleMenuViewMapper, RoleMenuView> implements IRoleMenuViewService {

    @Override
    public List<RoleMenuView> queryRouteByRole(String rolesId) {
        //动态路由-->查询父级菜单
        System.out.println("用户角色:"+rolesId);
        System.out.println("父级菜单");
        LambdaQueryWrapper<RoleMenuView> lqw = Wrappers.lambdaQuery();
        lqw.eq(RoleMenuView::getParentId,0);
        lqw.groupBy(RoleMenuView::getMenuName);
        lqw.in(StringUtils.isNotBlank(rolesId),RoleMenuView::getRoleId,rolesId);
        lqw.orderByAsc(RoleMenuView::getSortCode);
        List<RoleMenuView> roleMenuViews = baseMapper.selectList(lqw);
        if (roleMenuViews != null && roleMenuViews.size() > 0){
            roleMenuViews.forEach(this::queryChildren);
        }
        return roleMenuViews;
    }

    @Override
    public void queryChildren(RoleMenuView roleMenuView) {
        LambdaQueryWrapper<RoleMenuView> lqw = Wrappers.lambdaQuery();
        lqw.eq(RoleMenuView::getParentId,roleMenuView.getMenuId());
        lqw.groupBy(RoleMenuView::getMenuName);
        lqw.orderByAsc(RoleMenuView::getSortCode);
        List<RoleMenuView> roleMenuViews = baseMapper.selectList(lqw);
        roleMenuView.setChildren(roleMenuViews);
        //查询子菜单
        if (roleMenuViews != null && roleMenuViews.size() > 0){
            roleMenuViews.forEach(this::queryChildren);
        }
    }
}
