package com.cqie.crms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cqie.crms.bo.MenuInfoReqBo;
import com.cqie.crms.bo.QueryMenuInfoReqBo;
import com.cqie.crms.pojo.SysMenu;
import com.cqie.crms.mapper.SysMenuMapper;
import com.cqie.crms.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.vo.MenuVo;
import com.github.pagehelper.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-14
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Override
    public List<SysMenu> buildMenu() {
        //查询父级菜单
        System.out.println("获取菜单信息");
        LambdaQueryWrapper<SysMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysMenu::getParentId,0);
        lqw.or().isNull(SysMenu::getParentId).orderByAsc(SysMenu::getSortCode);
        lqw.groupBy(SysMenu::getName);
        List<SysMenu> menus = baseMapper.selectList(lqw);
        //查询子菜单
        if (menus != null && menus.size() > 0){
          menus.forEach(this::findAllChild);
        }
        return menus;
    }

    //查詢所有子菜單
    @Override
    public void findAllChild(SysMenu sysMenu) {
        LambdaQueryWrapper<SysMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysMenu::getParentId,sysMenu.getId())
                .orderByAsc(SysMenu::getSortCode);
        List<SysMenu> menus = baseMapper.selectList(lqw);
        sysMenu.setChildren(menus);
        //查询子菜单
        if (menus != null && menus.size() > 0){
            menus.forEach(this::findAllChild);
        }
    }

    //查询所有菜单列表
    @Override
    public List<SysMenu> getMenuList(QueryMenuInfoReqBo bo) {
        MyPageHelper.startPage(bo);
        System.out.println(bo.getName());
        LambdaQueryWrapper<SysMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getParentId()),SysMenu::getParentId,bo.getParentId());
        lqw.likeRight(StringUtils.isNotBlank(bo.getName()),SysMenu::getName,bo.getName())
                .orderByAsc(SysMenu::getSortCode);
        List<SysMenu> menus = baseMapper.selectList(lqw);
        return menus;
    }

    @Override
    public Boolean addMenu(MenuInfoReqBo bo) {
        //重复检查
        LambdaQueryWrapper<SysMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getName()),SysMenu::getName,bo.getName());
        SysMenu selectOne = baseMapper.selectOne(lqw);
        AssertUtil.isNull(selectOne,"该菜单已经存在");
        //新增
        SysMenu sysMenu = new SysMenu();
        sysMenu.setName(bo.getName());
        sysMenu.setAppUrl(bo.getAppUrl());
        sysMenu.setImgUrl(bo.getImgUrl());
        if (bo.getSortCode() == null){
            bo.setSortCode(0);
        }
        if (bo.getParentId() == null){
            bo.setParentId(Long.parseLong("0"));
        }
        sysMenu.setParentId(bo.getParentId());
        sysMenu.setSortCode(bo.getSortCode());
        int count = baseMapper.insert(sysMenu);
        AssertUtil.isTrue(count > 0,"新增菜单失败！");
        return count > 0;
    }

    @Override
    public Boolean updateMenu(MenuInfoReqBo bo) {
        //重复性检查
        AssertUtil.isNotNull(bo.getId(),"菜单ID不能为空");
        LambdaQueryWrapper<SysMenu> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysMenu::getId,bo.getId());
        SysMenu selectOne = baseMapper.selectOne(lqw);
        AssertUtil.isNotNull(selectOne,"该菜单不存在");
        //编辑
        if (bo.getSortCode() == null){
            bo.setSortCode(0);
        }
        if (bo.getParentId() == null){
            bo.setParentId(Long.parseLong("0"));
        }
        int update = baseMapper.update(null,new LambdaUpdateWrapper<SysMenu>()
        .set(StringUtils.isNotBlank(bo.getName()),SysMenu::getName,bo.getName())
        .set(StringUtils.isNotBlank(bo.getAppUrl()),SysMenu::getAppUrl,bo.getAppUrl())
        .set(StringUtils.isNotBlank(bo.getImgUrl()),SysMenu::getImgUrl,bo.getImgUrl())
        .set(bo.getSortCode() != null,SysMenu::getSortCode,bo.getSortCode())
        .set(bo.getParentId() != null,SysMenu::getParentId,bo.getParentId())
        .eq(SysMenu::getId,bo.getId()));
        AssertUtil.isTrue(update > 0, "编辑用户信息失败！");
        return update > 0;
    }

    @Override
    public Boolean deleteMenu(String[] menuId) {
        List<String> list = Arrays.asList(menuId);
        int count = baseMapper.deleteBatchIds(list);
        AssertUtil.isTrue(count > 0, "删除失败！");
        return count > 0;
    }

    //将实体转换为Vo类型
    private List<MenuVo> entity2Vo(Collection<SysMenu> collection) {
        List<MenuVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, MenuVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<SysMenu> page = (Page<SysMenu>) collection;
            Page<MenuVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }
}
