package com.cqie.crms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cqie.crms.bo.AddRoleReqBo;
import com.cqie.crms.bo.QueryRoleInfoReqBo;
import com.cqie.crms.pojo.SysRole;
import com.cqie.crms.mapper.SysRoleMapper;
import com.cqie.crms.service.IRoleMenuService;
import com.cqie.crms.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.service.IUserRoleService;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.vo.RoleMenuVo;
import com.cqie.crms.vo.RoleVo;
import com.cqie.crms.vo.UserRoleVo;
import com.github.pagehelper.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-09
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private IRoleMenuService iRoleMenuService;

    @Autowired
    private IUserRoleService iUserRoleService;


    //查询角色下拉列表
    @Override
    public List<RoleVo> getRoleList(QueryRoleInfoReqBo bo) {
    LambdaQueryWrapper<SysRole> lqw = Wrappers.lambdaQuery();
    lqw.eq(StringUtils.isNotBlank(bo.getName()),SysRole::getName,bo.getName());
    lqw.eq(bo.getStatus() != null,SysRole::getStatus,bo.getStatus());
    List<SysRole> sysRoles = baseMapper.selectList(lqw);
    return entity2Vo(sysRoles);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addRole(AddRoleReqBo bo) {
        //存在性检查
        LambdaQueryWrapper<SysRole> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getName()),SysRole::getName,bo.getName());

        SysRole selectOne = baseMapper.selectOne(lqw);
        AssertUtil.isNull(selectOne,"该角色已经存在");
        //新增
        SysRole sysRole = new SysRole();
        sysRole.setName(bo.getName());
        sysRole.setRemark(bo.getRemark());
        int count = baseMapper.insert(sysRole);
        AssertUtil.isTrue(count > 0,"新增角色失败！");
        RoleMenuVo roleMenuVo = new RoleMenuVo();
        roleMenuVo.setRoleId(sysRole.getId());
        roleMenuVo.setMenus(bo.getRole());
        Boolean bln = iRoleMenuService.addRoleMenu(roleMenuVo);
        return bln;
    }

    //编辑角色信息或权限
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateRole(AddRoleReqBo bo) {
        AssertUtil.isNotNull(bo.getId(), "角色ID不能为空");
        //更新的角色不能跟之前相同
        LambdaQueryWrapper<SysRole> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getId() != null,SysRole::getId,bo.getId());
        SysRole sysRole = baseMapper.selectOne(lqw);
        AssertUtil.isNotNull(sysRole,"该角色不存在！");
        //新增操作
        int count = baseMapper.update(null, new LambdaUpdateWrapper<SysRole>()
        .set(StringUtils.isNotBlank(bo.getName()),SysRole::getName,bo.getName())
        .set(SysRole::getRemark,bo.getRemark())
        .set(SysRole::getStatus,0)
                .eq(SysRole::getId,bo.getId()));
        AssertUtil.isTrue(count > 0,"编辑用户角色失败！");
        RoleMenuVo roleMenuVo = new RoleMenuVo();
        roleMenuVo.setRoleId(bo.getId());
        roleMenuVo.setMenus(bo.getRole());
        Boolean bln = iRoleMenuService.updateRoleMenu(roleMenuVo);
        return bln;
    }

    //删除角色将原有的角色抓换成游客身份
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteRole(String id) {
        AssertUtil.isNotNull(id, "角色ID不能为空");
        //获取当前用户角色
        Long roleId = Long.parseLong(id);
        Boolean bln = iUserRoleService.deleteUserRole(roleId);
        return bln;
    }

    //将实体转换为Vo类型
    private List<RoleVo> entity2Vo(Collection<SysRole> collection) {
        List<RoleVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, RoleVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<SysRole> page = (Page<SysRole>) collection;
            Page<RoleVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }
}
