package com.cqie.crms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.bo.AddUserReqBo;
import com.cqie.crms.bo.LoginReqBo;
import com.cqie.crms.bo.QueryUserInfoReqBo;
import com.cqie.crms.bo.UpdateUserReqBo;
import com.cqie.crms.enums.YesOrNo;
import com.cqie.crms.mapper.SysUserMapper;
import com.cqie.crms.pojo.SysUser;
import com.cqie.crms.service.ISysUserService;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.utils.MD5Util;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.utils.TokenUtil;
import com.cqie.crms.vo.LoginVo;
import com.cqie.crms.vo.UserRoleVo;
import com.cqie.crms.vo.UserVo;
import com.github.pagehelper.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-02
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private UserRoleServiceImpl userRoleService;

    @Override
    public LoginVo login(LoginReqBo bo) {
        AssertUtil.isNotBlank(bo.getAccount(), "登录账号不能为空！");
        AssertUtil.isNotBlank(bo.getPassword(), "登录密码不能为空！");
        LambdaQueryWrapper<SysUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysUser::getStatus, YesOrNo.YES);
        lqw.eq(SysUser::getAccount, bo.getAccount());
        SysUser sysUser = baseMapper.selectOne(lqw);
        AssertUtil.isNotNull(sysUser, "用户不存在或者已被禁用！");
        String md5Password = MD5Util.stringToMD5(bo.getPassword());
        AssertUtil.isTrue(md5Password.equals(sysUser.getPassword()), "密码不正确");
        String token = TokenUtil.sign(sysUser);
        baseMapper.update(null, new LambdaUpdateWrapper<SysUser>()
                .set(SysUser::getToken, token)
                .eq(SysUser::getId, sysUser.getId()));
        LoginVo loginVo = new LoginVo();
        loginVo.setToken(token);
        return loginVo;
    }

    @Override
    public void logOut() {

    }

    @Override
    public UserVo getInfoById(Long userId) {
        LambdaQueryWrapper<SysUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysUser::getId, userId);
        SysUser sysUser = baseMapper.selectById(userId);
        AssertUtil.isNotNull(sysUser, "用户不存在");
        UserVo user = BeanUtil.toBean(sysUser, UserVo.class);
        return user;
    }

    @Override
    public List<UserVo> getUserList(QueryUserInfoReqBo bo) {
        MyPageHelper.startPage(bo);
        LambdaQueryWrapper<SysUser> lqw = Wrappers.lambdaQuery();
        lqw.likeRight(StringUtils.isNotBlank(bo.getName()), SysUser::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getAccount()), SysUser::getAccount, bo.getAccount());
        lqw.eq(bo.getStatus() != null, SysUser::getStatus, bo.getStatus());
        List<SysUser> userVos = baseMapper.selectList(lqw);
        return entity2Vo(userVos);
    }

    //添加用户
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addUser(@RequestBody @Valid AddUserReqBo bo) {
        //1.判断添加用户账号是否存在
        LambdaQueryWrapper<SysUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysUser::getAccount, bo.getAccount());
        SysUser sysUser = baseMapper.selectOne(lqw);
        AssertUtil.isNull(sysUser, "该账号已经存在");
        //2.新增用户
        SysUser addUser = new SysUser();
        addUser.setAccount(bo.getAccount());
        addUser.setName(bo.getName());
        //设置默认密码为123456加密保存
        String password = MD5Util.stringToMD5("123456");
        addUser.setPassword(password);
        addUser.setStatus(bo.getStatus());
        int count = baseMapper.insert(addUser);
        AssertUtil.isTrue(count > 0, "新增用户失败！");
        UserRoleVo userRoleVo = new UserRoleVo();
        userRoleVo.setUserId(addUser.getId());
        userRoleVo.setRoleId(bo.getRolesId());
        Boolean bln = userRoleService.insertUserRole(userRoleVo);
        return bln;
    }

    //编辑用户信息
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean updateUser(UpdateUserReqBo bo) {
        AssertUtil.isNotNull(bo.getId(), "用户ID不能为空");
        //1.判断添加用户账号是否存在
        LambdaQueryWrapper<SysUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysUser::getAccount, bo.getAccount());
        SysUser sysUser = baseMapper.selectOne(lqw);
        AssertUtil.isNotNull(sysUser, "该账号不存在");
        //修改用户基本信息
        int update = baseMapper.update(null, new LambdaUpdateWrapper<SysUser>()
                .set(StringUtils.isNotBlank(bo.getName()), SysUser::getName, bo.getName())
                .set(StringUtils.isNotBlank(bo.getAccount()), SysUser::getAccount, bo.getAccount())
                .set(bo.getStatus() != null, SysUser::getStatus, bo.getStatus())
                .eq(SysUser::getId, bo.getId()));
        AssertUtil.isTrue(update > 0, "编辑用户信息失败！");
        //获取用户ID和角色ID
        UserRoleVo userRoleVo = new UserRoleVo();
        userRoleVo.setUserId(bo.getId());
        userRoleVo.setRoleId(bo.getRolesId());
        Boolean bln = userRoleService.updateUserRole(userRoleVo);
        return bln;
    }

    //批量删除
    @Override
    public Boolean deleteUserById(String[] userId) {
        List<String> list = Arrays.asList(userId);
        int count = baseMapper.deleteBatchIds(list);
        AssertUtil.isTrue(count > 0, "删除失败！");
        return count > 0;
    }

    //修改用户状态
    @Override
    public Boolean modifyStatus(String id, String status) {
        int update = baseMapper.update(null, new LambdaUpdateWrapper<SysUser>()
                .set(status != null, SysUser::getStatus, status)
                .eq(SysUser::getId, id));
        return update > 0;
    }

    private List<UserVo> entity2Vo(Collection<SysUser> collection) {
        List<UserVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, UserVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<SysUser> page = (Page<SysUser>) collection;
            Page<UserVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }
}
