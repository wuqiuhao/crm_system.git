package com.cqie.crms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cqie.crms.bo.QueryUserInfoReqBo;
import com.cqie.crms.pojo.SysUser;
import com.cqie.crms.pojo.UserInfoView;
import com.cqie.crms.mapper.UserInfoViewMapper;
import com.cqie.crms.service.IUserInfoViewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.utils.MyPageHelper;
import com.cqie.crms.vo.UserVo;
import com.github.pagehelper.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * VIEW 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-08
 */
@Service
public class UserInfoViewServiceImpl extends ServiceImpl<UserInfoViewMapper, UserInfoView> implements IUserInfoViewService {

    //获取登录用户信息
    @Override
    public UserVo getInfoById(Long userId) {
        LambdaQueryWrapper<UserInfoView> lqw = Wrappers.lambdaQuery();
        lqw.eq(UserInfoView::getId, userId);
        UserInfoView userInfoView = baseMapper.selectById(userId);
        AssertUtil.isNotNull(userInfoView, "用户不存在");
        UserVo user = BeanUtil.toBean(userInfoView, UserVo.class);
        return user;
    }
    //查询用户列表信息
    @Override
    public List<UserVo> getUserList(QueryUserInfoReqBo bo) {
        MyPageHelper.startPage(bo);
        LambdaQueryWrapper<UserInfoView> lqw = Wrappers.lambdaQuery();
        lqw.likeRight(StringUtils.isNotBlank(bo.getName()), UserInfoView::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getAccount()), UserInfoView::getAccount, bo.getAccount());
        lqw.eq(bo.getStatus() != null, UserInfoView::getStatus, bo.getStatus());
        if(bo.getSort() == null){
            bo.setSort(0);
        }
        lqw.orderByAsc(bo.getSort() == 0,UserInfoView::getId);
        lqw.orderByDesc(bo.getSort() == 1,UserInfoView::getId);
        List<UserInfoView> userVos = baseMapper.selectList(lqw);
        return entity2Vo(userVos);
    }

    //将实体转换为Vo类型
    private List<UserVo> entity2Vo(Collection<UserInfoView> collection) {
        List<UserVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, UserVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<UserInfoView> page = (Page<UserInfoView>) collection;
            Page<UserVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page, pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }
}
