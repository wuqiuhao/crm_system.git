package com.cqie.crms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.cqie.crms.pojo.UserRole;
import com.cqie.crms.mapper.UserRoleMapper;
import com.cqie.crms.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqie.crms.utils.AssertUtil;
import com.cqie.crms.vo.UserRoleVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2021-09-10
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    //新增用户角色
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertUserRole(UserRoleVo vo) {
        UserRole userRole = new UserRole();
        //获取用户ID和角色ID
        Long userId = vo.getUserId();
        Long[] roleIds = vo.getRoleId();
        System.out.println("用户ID："+userId);
        System.out.println("角色ID："+roleIds);
        //将新增用户的id装到实体
        userRole.setUserId(userId);
        int i;
        for (i = 0;i < roleIds.length;i++){
           // 将角色id数组添加到实体
        Long roleId = roleIds[i];
        userRole.setRoleId(roleId);
        //循环遍历用户角色插入到UserRole表
        baseMapper.insert(userRole);
        }
        if (i == roleIds.length){
            return true;
        }else {
            return false;
        }
    }
    //编辑用户角色
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserRole(UserRoleVo vo) {
        System.out.println(vo.getUserId());
        System.out.println(vo.getRoleId());
        //获取用户ID和角色ID
        Long userId = vo.getUserId();
        Long[] roleIds = vo.getRoleId();
        //先将该用户原有的角色删除
        baseMapper.delete(new LambdaUpdateWrapper<UserRole>().eq(UserRole::getUserId,userId));
        //再将修改的角色写入表中
        UserRole userRole = new UserRole();
        userRole.setUserId(userId);
        //循环遍历插入用户角色
        int i;
        for (i = 0;i < roleIds.length;i++){
            Long roleId = roleIds[i];
            userRole.setRoleId(roleId);
            baseMapper.insert(userRole);
        }
        if (i == roleIds.length){
            return true;
        }else {
            return false;
        }
    }

   //删除角色将用户角色设置为游客身份
    @Override
    public Boolean deleteUserRole(Long roleId) {
        //当前用户角色id
        int count = baseMapper.update(null, new LambdaUpdateWrapper<UserRole>()
        .set(roleId != null,UserRole::getRoleId,2)
        .eq(UserRole::getRoleId,roleId));
        AssertUtil.isTrue(count > 0,"删除角色失败！");
        return count > 0;
    }

}
