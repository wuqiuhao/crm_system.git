package com.cqie.crms.utils;

import com.cqie.crms.enums.RtnCode;
import com.cqie.crms.handler.CustomException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * @Author: alex
 * @Date: 2021/09/06/9:03
 * @Description:
 */
public class AssertUtil {
    /**
     * 对象为空时
     *
     * @param obj
     * @param message
     */
    public static void isNotNull(Object obj, RtnCode codeEnum, String message) {
        if (obj == null) {
            message(codeEnum, message);
        }
    }

    /**
     * 对象为空时
     *
     * @param obj
     * @param message
     */
    public static void isNotNull(Object obj, String message) {
        isNotNull(obj, null, message);
    }

    /**
     * 对象为空时
     *
     * @param obj
     * @param codeEnum
     */
    public static void isNotNull(Object obj, RtnCode codeEnum) {
        isNotNull(obj, codeEnum, null);
    }

    /**
     * 对象为空时
     *
     * @param obj
     */
    public static void isNotNull(Object obj) {
        isNotNull(obj, null, null);
    }

    /**
     * 对象不为空时
     *
     * @param obj
     * @param message
     */
    public static void isNull(Object obj, RtnCode codeEnum, String message) {
        if (obj != null) {
            message(codeEnum, message);
        }
    }

    /**
     * 对象不为空时
     *
     * @param obj
     * @param codeEnum
     */
    public static void isNull(Object obj, RtnCode codeEnum) {
        isNull(obj, codeEnum, null);
    }

    /**
     * 对象不为空时
     *
     * @param obj
     * @param message
     */
    public static void isNull(Object obj, String message) {
        isNull(obj, null, message);
    }

    /**
     * 对象不为空时
     *
     * @param obj
     */
    public static void isNull(Object obj) {
        isNull(obj, null, null);
    }

    /**
     * 集合不为空时
     *
     * @param coll
     * @param message
     */
    public static void isNotEmpty(Collection coll, RtnCode codeEnum, String message) {
        if (CollectionUtils.isEmpty(coll)) {
            message(codeEnum, message);
        }
    }

    /**
     * 集合不为空时
     *
     * @param coll
     * @param message
     */
    public static void isNotEmpty(Collection coll, String message) {
        isNotEmpty(coll, null, message);
    }

    /**
     * 集合不为空时
     *
     * @param coll
     * @param codeEnum
     */
    public static void isNotEmpty(Collection coll, RtnCode codeEnum) {
        isNotEmpty(coll, codeEnum, null);
    }

    /**
     * 集合不为空时
     */
    public static void isNotEmpty(Collection coll) {
        isNotEmpty(coll, null, null);
    }

    /**
     * 集合为空时
     *
     * @param coll
     * @param codeEnum
     */
    public static void isEmpty(Collection coll, RtnCode codeEnum, String message) {
        if (!CollectionUtils.isEmpty(coll)) {
            message(codeEnum, message);
        }
    }

    /**
     * 集合为空时
     *
     * @param coll
     * @param message
     */
    public static void isEmpty(Collection coll, String message) {
        isEmpty(coll, null, message);
    }

    /**
     * 集合为空时
     *
     * @param coll
     * @param codeEnum
     */
    public static void isEmpty(Collection coll, RtnCode codeEnum) {
        isEmpty(coll, codeEnum, null);
    }

    /**
     * 集合为空时
     *
     * @param coll
     */
    public static void isEmpty(Collection coll) {
        isEmpty(coll, null, null);
    }

    /**
     * 字符串为空
     *
     * @param str
     * @param message
     */
    public static void isBlank(String str, RtnCode codeEnum, String message) {
        if (StringUtils.isNotBlank(str)) {
            message(codeEnum, message);
        }
    }

    /**
     * 字符串为空
     *
     * @param str
     * @param message
     */
    public static void isBlank(String str, String message) {
        isBlank(str, null, message);
    }

    /**
     * 字符串为空
     *
     * @param str
     * @param codeEnum
     */
    public static void isBlank(String str, RtnCode codeEnum) {
        isBlank(str, codeEnum, null);
    }

    /**
     * 字符串为空
     *
     * @param str
     */
    public static void isBlank(String str) {
        isBlank(str, null, null);
    }

    /**
     * 字符串不为空
     *
     * @param str
     * @param message
     */
    public static void isNotBlank(String str, RtnCode codeEnum, String message) {
        if (StringUtils.isBlank(str)) {
            message(codeEnum, message);
        }
    }

    /**
     * 字符串不为空
     *
     * @param str
     * @param message
     */
    public static void isNotBlank(String str, String message) {
        isNotBlank(str, null, message);
    }

    /**
     * 字符串不为空
     *
     * @param str
     * @param codeEnum
     */
    public static void isNotBlank(String str, RtnCode codeEnum) {
        isNotBlank(str, codeEnum, null);
    }

    /**
     * 字符串不为空
     *
     * @param str
     */
    public static void isNotBlank(String str) {
        isNotBlank(str, null, null);
    }

    /**
     * 判断为真时
     *
     * @param condition
     * @param message
     * @
     */
    public static void isTrue(boolean condition, RtnCode codeEnum, String message) {
        if (!condition) {
            message(codeEnum, message);
        }
    }

    /**
     * 判断为真时
     *
     * @param condition
     * @param message
     * @
     */
    public static void isTrue(boolean condition, String message) {
        isTrue(condition, null, message);
    }

    /**
     * 判断为真时
     *
     * @param condition
     * @param codeEnum
     * @
     */
    public static void isTrue(boolean condition, RtnCode codeEnum) {
        isTrue(condition, codeEnum, null);
    }

    /**
     * 判断为真时
     *
     * @param condition
     * @
     */
    public static void isTrue(boolean condition) {
        isTrue(condition, null, null);
    }

    /**
     * 判断为假时
     *
     * @param condition
     * @param message
     * @
     */
    public static void isFalse(boolean condition, RtnCode codeEnum, String message) {
        if (condition) {
            message(codeEnum, message);
        }
    }

    /**
     * 判断为假时
     *
     * @param condition
     * @param message
     * @
     */
    public static void isFalse(boolean condition, String message) {
        isFalse(condition, null, message);
    }

    /**
     * 判断为假时
     *
     * @param condition
     * @param codeEnum
     * @
     */
    public static void isFalse(boolean condition, RtnCode codeEnum) {
        isFalse(condition, codeEnum, null);
    }

    /**
     * 判断为假时
     *
     * @param condition
     * @
     */
    public static void isFalse(boolean condition) {
        isFalse(condition, null, null);
    }


    /**
     * 抛出自定义异常
     *
     * @param code
     * @param message
     */
    public static void message(RtnCode code, String message) {
        throw new CustomException(message);
    }

}

