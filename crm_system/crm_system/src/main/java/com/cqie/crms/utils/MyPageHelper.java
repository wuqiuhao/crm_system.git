package com.cqie.crms.utils;

import cn.hutool.core.lang.Validator;
import com.cqie.crms.bo.PageReqBo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class MyPageHelper {
    public static void startPage(PageReqBo bo) {
        Integer pageNum = bo.getPageNum();
        Integer pageSize = bo.getPageSize();
        if (Validator.isNotNull(pageNum) && Validator.isNotNull(pageSize)) {
            if (StringUtils.isNotBlank(bo.getOrderBy())) {
                String orderBy = SqlUtil.escapeOrderBySql(bo.getOrderBy());
                PageHelper.startPage(pageNum, pageSize, orderBy);
            } else {
                PageHelper.startPage(pageNum, pageSize);
            }
        }
    }

    public static PageInfo returnPage(List resultList) {
//        Page page = (Page) resultList;
//        Page pageVo = new Page<>();
//        BeanUtil.copyProperties(page, pageVo);
//        pageVo.addAll(resultList);
        PageInfo pageInfo = new PageInfo<>(resultList);
        return pageInfo;
    }
}
