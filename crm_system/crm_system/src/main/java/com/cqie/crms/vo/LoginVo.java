package com.cqie.crms.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/07/21:01
 * @Description:
 */
@Data
@ApiModel("返回token")
public class LoginVo {

    /**
     * token
     */
    String token;
}
