package com.cqie.crms.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: alex
 * @Date: 2021/09/14/10:51
 * @Description:
 */
@Data
@ApiModel("菜单返回实体")
public class MenuVo {
    /**
     * 菜单id
     */
    private Long id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 程序路径
     */
    private String appUrl;

    /**
     * 图标路径
     */
    private String imgUrl;

    /**
     * 排序码
     */
    @TableField("sortCode")
    private Integer sortCode;

    /**
     * 上级菜单Id
     */
    private Long parentId;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remark;

    private List<MenuVo> children;
}
