package com.cqie.crms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("分页查询返回实体")
public class PageVo<T> {

    @ApiModelProperty("总条数")
    private Integer total;

    @ApiModelProperty("总页数")
    private Integer pages;

    @ApiModelProperty("数据行")
    private List<T> dataList;
}
