package com.cqie.crms.vo;

import cn.hutool.core.lang.Validator;
import com.cqie.crms.enums.RtnCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 操作消息提醒
 *
 * @author plastic
 */
@Data
@ApiModel("返回结构体")
public class ResultVo<T> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    @ApiModelProperty(required = true, value = "返回码", example = "000000:成功，其他均为异常")
    private String code;

    /**
     * 返回内容
     */
    @ApiModelProperty(required = false, value = "返回消息")
    private String msg;

    /**
     * 数据对象
     */
    @ApiModelProperty(required = false, value = "返回数据结果")
    private T data;

    /**
     * 数据对象列表
     */
    @ApiModelProperty(required = false, value = "返回数据结果")
    private List<T> dataList;

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public ResultVo() {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     */
    public ResultVo(String code, String msg) {
        this. code = code;
        this.msg = msg;
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param dataList 列表
     */
    public ResultVo(String code, String msg,List<T> dataList) {
        this. code = code;
        this.msg = msg;
        this.dataList = dataList;
    }


    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param data 数据对象
     */
    public ResultVo(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        if (Validator.isNotNull(data)) {
            this.data = data;
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ResultVo<Void> success() {
        return ResultVo.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static <T> ResultVo<T> success(T data) {
        return ResultVo.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static ResultVo<Void> success(String msg) {
        return ResultVo.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> ResultVo<T> success(String msg, T data) {
        return new ResultVo(RtnCode.SUCCESS.getCode(), msg, data);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param dataList 数据对象
     * @return 成功消息
     */
    public static <T> ResultVo<T> success(String msg, List<T> dataList) {
        return new ResultVo(RtnCode.SUCCESS.getCode(), msg, dataList);
    }

    /**
     * 返回错误消息
     *
     * @return
     */
    public static ResultVo<Void> error() {
        return ResultVo.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResultVo<Void> error(String msg) {
        return ResultVo.error(RtnCode.UNKNOWN_EXCEPTION.getCode(), msg);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> ResultVo<T> error(String msg, T data) {
        return new ResultVo(RtnCode.UNKNOWN_EXCEPTION.getCode(), msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 警告消息
     */
    public static ResultVo<Void> error(String code, String msg) {
        return new ResultVo(code, msg, null);
    }
}
