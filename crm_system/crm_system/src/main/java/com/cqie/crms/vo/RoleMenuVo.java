package com.cqie.crms.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/16/23:43
 * @Description:
 */
@Data
@ApiModel("角色菜单VO")
public class RoleMenuVo {
    /**
     * 角色ID
     */
    Long roleId;
    /**
     * 菜单
     */
    Long[] menus;
}
