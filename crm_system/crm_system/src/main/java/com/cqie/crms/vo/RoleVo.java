package com.cqie.crms.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: alex
 * @Date: 2021/09/09/11:14
 * @Description:
 */
@Data
@ApiModel("角色下拉列表实体")
public class RoleVo {

    /**
     * 角色id
     */
    private Long id;

    /**
     * 角色名
     */
    private String name;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remark;
}
