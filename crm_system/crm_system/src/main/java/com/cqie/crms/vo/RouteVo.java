package com.cqie.crms.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cqie.crms.pojo.SysMenu;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: alex
 * @Date: 2021/09/18/8:55
 * @Description:
 */
@Data
@ApiModel("动态菜单路由返回实体")
public class RouteVo {

    /**
     * 菜单id
     */
    private Long menu_id;

    /**
     * 菜单名称
     */
    private String menu_name;

    /**
     * 程序路径
     */
    private String appUrl;

    /**
     * 图标路径
     */
    private String imgUrl;

    /**
     * 排序码
     */
    private Integer sortCode;

    /**
     * 上级菜单Id
     */
    private Long parentId;


    private List<SysMenu> children;
}
