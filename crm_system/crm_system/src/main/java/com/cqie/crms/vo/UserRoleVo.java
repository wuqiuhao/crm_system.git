package com.cqie.crms.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author: alex
 * @Date: 2021/09/10/10:16
 * @Description:
 */
@Data
@ApiModel("用户角色实体")
public class UserRoleVo {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 角色id
     */
    private Long[] roleId;
}
